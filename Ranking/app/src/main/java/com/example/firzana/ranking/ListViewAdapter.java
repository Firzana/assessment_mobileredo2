package com.example.firzana.ranking;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Firzana on 4/4/2018.
 */

public class ListViewAdapter extends BaseAdapter{
    Context context;
    List<Website> tempWebsiteList;

    public ListViewAdapter(List<Website> tempWebsiteList, Context context) {
        this.context = context;
        this.tempWebsiteList = tempWebsiteList;
    }

    @Override
    public int getCount() {
        return this.tempWebsiteList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.tempWebsiteList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewItem viewItem = null;

        if(convertView == null)
        {
            viewItem = new ViewItem();
            LayoutInflater layoutInfiater = (LayoutInflater)this.context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInfiater.inflate(R.layout.listview_items, null);
            //viewItem.tv_id = (TextView)convertView.findViewById(R.id.tv_id);
            viewItem.tv_websitename = (TextView)convertView.findViewById(R.id.tv_websitename);
            viewItem.tv_visitdate = (TextView)convertView.findViewById(R.id.tv_visitdate);
            viewItem.tv_totalvisits = (TextView)convertView.findViewById(R.id.tv_totalvisits);
            convertView.setTag(viewItem);
        }
        else
        {
            viewItem = (ViewItem) convertView.getTag();
        }

        //viewItem.tv_id.setText(tempWebsiteList.get(position).Website_ID);
        viewItem.tv_websitename.setText(tempWebsiteList.get(position).Website_Name);
        viewItem.tv_visitdate.setText(tempWebsiteList.get(position).Website_VisitDate);
        viewItem.tv_totalvisits.setText(tempWebsiteList.get(position).Website_TotalVisits);

        return convertView;
    }
}
class ViewItem
{
    //TextView tv_id;
    TextView tv_websitename;
    TextView tv_visitdate;
    TextView tv_totalvisits;
}
