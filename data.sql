-- phpMyAdmin SQL Dump
-- version 4.7.0-rc1
-- https://www.phpmyadmin.net/
--
-- Host: mysql5018.site4now.net
-- Generation Time: Apr 09, 2018 at 01:47 PM
-- Server version: 5.6.26-log
-- PHP Version: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_a38e84_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE `data` (
  `id_website` int(10) NOT NULL,
  `website_name` varchar(12) DEFAULT NULL,
  `visit_date` date DEFAULT NULL,
  `total_visits` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`id_website`, `website_name`, `visit_date`, `total_visits`) VALUES
(1, 'amazon', '2017-03-01', '1000'),
(2, 'yahoo', '2017-03-01', '3296'),
(3, 'lazada', '2017-03-01', '3346'),
(4, 'facebook', '2017-03-01', '1598'),
(5, 'mudah', '2017-03-01', '2964'),
(6, 'ibilik', '2017-03-01', '2188'),
(7, 'foodpanda', '2017-03-01', '3187'),
(8, 'google', '2017-03-01', '5899'),
(9, 'youtube', '2017-03-01', '2495'),
(10, 'sports', '2017-03-01', '3491'),
(11, 'amazon', '2017-03-02', '3491'),
(12, 'yahoo', '2017-03-02', '2964'),
(13, 'lazada', '2017-03-02', '2188'),
(14, 'facebook', '2017-03-02', '3491'),
(15, 'mudah', '2017-03-02', '1598'),
(16, 'ibilik', '2017-03-02', '1000'),
(17, 'foodpanda', '2017-03-02', '2495'),
(18, 'google', '2017-03-02', '3296'),
(19, 'youtube', '2017-03-02', '1598'),
(20, 'sports', '2017-03-02', '2964'),
(21, 'amazon', '2017-03-03', '1598'),
(22, 'yahoo', '2017-03-03', '2188'),
(23, 'lazada', '2017-03-03', '3296'),
(24, 'facebook', '2017-03-03', '3187'),
(25, 'mudah', '2017-03-03', '3491'),
(26, 'ibilik', '2017-03-03', '5899'),
(27, 'foodpanda', '2017-03-03', '3346'),
(28, 'google', '2017-03-03', '1598'),
(29, 'youtube', '2017-03-03', '2188'),
(30, 'sports', '2017-03-03', '1598'),
(31, 'amazon', '2017-03-04', '2964'),
(32, 'yahoo', '2017-03-04', '2495'),
(33, 'lazada', '2017-03-04', '3187'),
(34, 'facebook', '2017-03-04', '1598'),
(35, 'mudah', '2017-03-04', '2964'),
(36, 'ibilik', '2017-03-04', '2495'),
(37, 'foodpanda', '2017-03-04', '3491'),
(38, 'google', '2017-03-04', '2964'),
(39, 'youtube', '2017-03-04', '5899'),
(40, 'sports', '2017-03-04', '2495'),
(41, 'amazon', '2017-03-05', '1598'),
(42, 'yahoo', '2017-03-05', '2964'),
(43, 'lazada', '2017-03-05', '3187'),
(44, 'facebook', '2017-03-05', '2188'),
(45, 'mudah', '2017-03-05', '3187'),
(46, 'ibilik', '2017-03-05', '3296'),
(47, 'foodpanda', '2017-03-05', '1598'),
(48, 'google', '2017-03-05', '2495'),
(49, 'youtube', '2017-03-05', '1000'),
(50, 'sports', '2017-03-05', '2188'),
(51, 'amazon', '2017-03-06', '2964'),
(52, 'yahoo', '2017-03-06', '5899'),
(53, 'lazada', '2017-03-06', '2495'),
(54, 'facebook', '2017-03-06', '2495'),
(55, 'mudah', '2017-03-06', '2964'),
(56, 'ibilik', '2017-03-06', '1598'),
(57, 'foodpanda', '2017-03-06', '2188'),
(58, 'google', '2017-03-06', '1598'),
(59, 'youtube', '2017-03-06', '3491'),
(60, 'sports', '2017-03-06', '5899'),
(61, 'amazon', '2017-03-07', '3491'),
(62, 'yahoo', '2017-03-07', '2964'),
(63, 'lazada', '2017-03-07', '3346'),
(64, 'facebook', '2017-03-07', '1598'),
(65, 'mudah', '2017-03-07', '2495'),
(66, 'ibilik', '2017-03-07', '3491'),
(67, 'foodpanda', '2017-03-07', '3187'),
(68, 'google', '2017-03-07', '2188'),
(69, 'youtube', '2017-03-07', '3187'),
(70, 'sports', '2017-03-07', '3296');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`id_website`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data`
--
ALTER TABLE `data`
  MODIFY `id_website` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
